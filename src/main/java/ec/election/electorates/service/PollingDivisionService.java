package ec.election.electorates.service;

import ec.election.electorates.domain.PollingDivision;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link PollingDivision}.
 */
public interface PollingDivisionService {

    /**
     * Save a pollingDivision.
     *
     * @param pollingDivision the entity to save.
     * @return the persisted entity.
     */
    PollingDivision save(PollingDivision pollingDivision);

    /**
     * Get all the pollingDivisions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PollingDivision> findAll(Pageable pageable);


    /**
     * Get the "id" pollingDivision.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PollingDivision> findOne(Long id);

    /**
     * Delete the "id" pollingDivision.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
