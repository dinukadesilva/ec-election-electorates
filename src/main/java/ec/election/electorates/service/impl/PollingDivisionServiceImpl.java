package ec.election.electorates.service.impl;

import ec.election.electorates.service.PollingDivisionService;
import ec.election.electorates.domain.PollingDivision;
import ec.election.electorates.repository.PollingDivisionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link PollingDivision}.
 */
@Service
@Transactional
public class PollingDivisionServiceImpl implements PollingDivisionService {

    private final Logger log = LoggerFactory.getLogger(PollingDivisionServiceImpl.class);

    private final PollingDivisionRepository pollingDivisionRepository;

    public PollingDivisionServiceImpl(PollingDivisionRepository pollingDivisionRepository) {
        this.pollingDivisionRepository = pollingDivisionRepository;
    }

    /**
     * Save a pollingDivision.
     *
     * @param pollingDivision the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PollingDivision save(PollingDivision pollingDivision) {
        log.debug("Request to save PollingDivision : {}", pollingDivision);
        return pollingDivisionRepository.save(pollingDivision);
    }

    /**
     * Get all the pollingDivisions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PollingDivision> findAll(Pageable pageable) {
        log.debug("Request to get all PollingDivisions");
        return pollingDivisionRepository.findAll(pageable);
    }


    /**
     * Get one pollingDivision by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PollingDivision> findOne(Long id) {
        log.debug("Request to get PollingDivision : {}", id);
        return pollingDivisionRepository.findById(id);
    }

    /**
     * Delete the pollingDivision by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PollingDivision : {}", id);
        pollingDivisionRepository.deleteById(id);
    }
}
