package ec.election.electorates.service.impl;

import ec.election.electorates.service.ElectoralDistrictService;
import ec.election.electorates.domain.ElectoralDistrict;
import ec.election.electorates.repository.ElectoralDistrictRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ElectoralDistrict}.
 */
@Service
@Transactional
public class ElectoralDistrictServiceImpl implements ElectoralDistrictService {

    private final Logger log = LoggerFactory.getLogger(ElectoralDistrictServiceImpl.class);

    private final ElectoralDistrictRepository electoralDistrictRepository;

    public ElectoralDistrictServiceImpl(ElectoralDistrictRepository electoralDistrictRepository) {
        this.electoralDistrictRepository = electoralDistrictRepository;
    }

    /**
     * Save a electoralDistrict.
     *
     * @param electoralDistrict the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ElectoralDistrict save(ElectoralDistrict electoralDistrict) {
        log.debug("Request to save ElectoralDistrict : {}", electoralDistrict);
        return electoralDistrictRepository.save(electoralDistrict);
    }

    /**
     * Get all the electoralDistricts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ElectoralDistrict> findAll(Pageable pageable) {
        log.debug("Request to get all ElectoralDistricts");
        return electoralDistrictRepository.findAll(pageable);
    }


    /**
     * Get one electoralDistrict by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ElectoralDistrict> findOne(Long id) {
        log.debug("Request to get ElectoralDistrict : {}", id);
        return electoralDistrictRepository.findById(id);
    }

    /**
     * Delete the electoralDistrict by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ElectoralDistrict : {}", id);
        electoralDistrictRepository.deleteById(id);
    }
}
