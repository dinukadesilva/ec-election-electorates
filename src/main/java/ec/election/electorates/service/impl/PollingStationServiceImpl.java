package ec.election.electorates.service.impl;

import ec.election.electorates.service.PollingStationService;
import ec.election.electorates.domain.PollingStation;
import ec.election.electorates.repository.PollingStationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link PollingStation}.
 */
@Service
@Transactional
public class PollingStationServiceImpl implements PollingStationService {

    private final Logger log = LoggerFactory.getLogger(PollingStationServiceImpl.class);

    private final PollingStationRepository pollingStationRepository;

    public PollingStationServiceImpl(PollingStationRepository pollingStationRepository) {
        this.pollingStationRepository = pollingStationRepository;
    }

    /**
     * Save a pollingStation.
     *
     * @param pollingStation the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PollingStation save(PollingStation pollingStation) {
        log.debug("Request to save PollingStation : {}", pollingStation);
        return pollingStationRepository.save(pollingStation);
    }

    /**
     * Get all the pollingStations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PollingStation> findAll(Pageable pageable) {
        log.debug("Request to get all PollingStations");
        return pollingStationRepository.findAll(pageable);
    }


    /**
     * Get one pollingStation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PollingStation> findOne(Long id) {
        log.debug("Request to get PollingStation : {}", id);
        return pollingStationRepository.findById(id);
    }

    /**
     * Delete the pollingStation by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PollingStation : {}", id);
        pollingStationRepository.deleteById(id);
    }
}
