package ec.election.electorates.service;

import ec.election.electorates.domain.PollingStation;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link PollingStation}.
 */
public interface PollingStationService {

    /**
     * Save a pollingStation.
     *
     * @param pollingStation the entity to save.
     * @return the persisted entity.
     */
    PollingStation save(PollingStation pollingStation);

    /**
     * Get all the pollingStations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PollingStation> findAll(Pageable pageable);


    /**
     * Get the "id" pollingStation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PollingStation> findOne(Long id);

    /**
     * Delete the "id" pollingStation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
