package ec.election.electorates.service;

import ec.election.electorates.domain.ElectoralDistrict;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link ElectoralDistrict}.
 */
public interface ElectoralDistrictService {

    /**
     * Save a electoralDistrict.
     *
     * @param electoralDistrict the entity to save.
     * @return the persisted entity.
     */
    ElectoralDistrict save(ElectoralDistrict electoralDistrict);

    /**
     * Get all the electoralDistricts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ElectoralDistrict> findAll(Pageable pageable);


    /**
     * Get the "id" electoralDistrict.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ElectoralDistrict> findOne(Long id);

    /**
     * Delete the "id" electoralDistrict.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
