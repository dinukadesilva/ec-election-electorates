/**
 * View Models used by Spring MVC REST controllers.
 */
package ec.election.electorates.web.rest.vm;
