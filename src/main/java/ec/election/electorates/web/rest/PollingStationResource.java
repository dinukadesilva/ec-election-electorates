package ec.election.electorates.web.rest;

import ec.election.electorates.domain.PollingStation;
import ec.election.electorates.service.PollingStationService;
import ec.election.electorates.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ec.election.electorates.domain.PollingStation}.
 */
@RestController
@RequestMapping("/api")
public class PollingStationResource {

    private final Logger log = LoggerFactory.getLogger(PollingStationResource.class);

    private static final String ENTITY_NAME = "pollingStation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PollingStationService pollingStationService;

    public PollingStationResource(PollingStationService pollingStationService) {
        this.pollingStationService = pollingStationService;
    }

    /**
     * {@code POST  /polling-stations} : Create a new pollingStation.
     *
     * @param pollingStation the pollingStation to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new pollingStation, or with status {@code 400 (Bad Request)} if the pollingStation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/polling-stations")
    public ResponseEntity<PollingStation> createPollingStation(@RequestBody PollingStation pollingStation) throws URISyntaxException {
        log.debug("REST request to save PollingStation : {}", pollingStation);
        if (pollingStation.getId() != null) {
            throw new BadRequestAlertException("A new pollingStation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PollingStation result = pollingStationService.save(pollingStation);
        return ResponseEntity.created(new URI("/api/polling-stations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /polling-stations} : Updates an existing pollingStation.
     *
     * @param pollingStation the pollingStation to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pollingStation,
     * or with status {@code 400 (Bad Request)} if the pollingStation is not valid,
     * or with status {@code 500 (Internal Server Error)} if the pollingStation couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/polling-stations")
    public ResponseEntity<PollingStation> updatePollingStation(@RequestBody PollingStation pollingStation) throws URISyntaxException {
        log.debug("REST request to update PollingStation : {}", pollingStation);
        if (pollingStation.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PollingStation result = pollingStationService.save(pollingStation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, pollingStation.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /polling-stations} : get all the pollingStations.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of pollingStations in body.
     */
    @GetMapping("/polling-stations")
    public ResponseEntity<List<PollingStation>> getAllPollingStations(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of PollingStations");
        Page<PollingStation> page = pollingStationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /polling-stations/:id} : get the "id" pollingStation.
     *
     * @param id the id of the pollingStation to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the pollingStation, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/polling-stations/{id}")
    public ResponseEntity<PollingStation> getPollingStation(@PathVariable Long id) {
        log.debug("REST request to get PollingStation : {}", id);
        Optional<PollingStation> pollingStation = pollingStationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(pollingStation);
    }

    /**
     * {@code DELETE  /polling-stations/:id} : delete the "id" pollingStation.
     *
     * @param id the id of the pollingStation to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/polling-stations/{id}")
    public ResponseEntity<Void> deletePollingStation(@PathVariable Long id) {
        log.debug("REST request to delete PollingStation : {}", id);
        pollingStationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
