package ec.election.electorates.web.rest;

import ec.election.electorates.domain.PollingDivision;
import ec.election.electorates.service.PollingDivisionService;
import ec.election.electorates.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ec.election.electorates.domain.PollingDivision}.
 */
@RestController
@RequestMapping("/api")
public class PollingDivisionResource {

    private final Logger log = LoggerFactory.getLogger(PollingDivisionResource.class);

    private static final String ENTITY_NAME = "pollingDivision";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PollingDivisionService pollingDivisionService;

    public PollingDivisionResource(PollingDivisionService pollingDivisionService) {
        this.pollingDivisionService = pollingDivisionService;
    }

    /**
     * {@code POST  /polling-divisions} : Create a new pollingDivision.
     *
     * @param pollingDivision the pollingDivision to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new pollingDivision, or with status {@code 400 (Bad Request)} if the pollingDivision has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/polling-divisions")
    public ResponseEntity<PollingDivision> createPollingDivision(@RequestBody PollingDivision pollingDivision) throws URISyntaxException {
        log.debug("REST request to save PollingDivision : {}", pollingDivision);
        if (pollingDivision.getId() != null) {
            throw new BadRequestAlertException("A new pollingDivision cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PollingDivision result = pollingDivisionService.save(pollingDivision);
        return ResponseEntity.created(new URI("/api/polling-divisions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /polling-divisions} : Updates an existing pollingDivision.
     *
     * @param pollingDivision the pollingDivision to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pollingDivision,
     * or with status {@code 400 (Bad Request)} if the pollingDivision is not valid,
     * or with status {@code 500 (Internal Server Error)} if the pollingDivision couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/polling-divisions")
    public ResponseEntity<PollingDivision> updatePollingDivision(@RequestBody PollingDivision pollingDivision) throws URISyntaxException {
        log.debug("REST request to update PollingDivision : {}", pollingDivision);
        if (pollingDivision.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PollingDivision result = pollingDivisionService.save(pollingDivision);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, pollingDivision.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /polling-divisions} : get all the pollingDivisions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of pollingDivisions in body.
     */
    @GetMapping("/polling-divisions")
    public ResponseEntity<List<PollingDivision>> getAllPollingDivisions(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of PollingDivisions");
        Page<PollingDivision> page = pollingDivisionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /polling-divisions/:id} : get the "id" pollingDivision.
     *
     * @param id the id of the pollingDivision to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the pollingDivision, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/polling-divisions/{id}")
    public ResponseEntity<PollingDivision> getPollingDivision(@PathVariable Long id) {
        log.debug("REST request to get PollingDivision : {}", id);
        Optional<PollingDivision> pollingDivision = pollingDivisionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(pollingDivision);
    }

    /**
     * {@code DELETE  /polling-divisions/:id} : delete the "id" pollingDivision.
     *
     * @param id the id of the pollingDivision to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/polling-divisions/{id}")
    public ResponseEntity<Void> deletePollingDivision(@PathVariable Long id) {
        log.debug("REST request to delete PollingDivision : {}", id);
        pollingDivisionService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
