package ec.election.electorates.web.rest;

import ec.election.electorates.domain.ElectoralDistrict;
import ec.election.electorates.service.ElectoralDistrictService;
import ec.election.electorates.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ec.election.electorates.domain.ElectoralDistrict}.
 */
@RestController
@RequestMapping("/api")
public class ElectoralDistrictResource {

    private final Logger log = LoggerFactory.getLogger(ElectoralDistrictResource.class);

    private static final String ENTITY_NAME = "electoralDistrict";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ElectoralDistrictService electoralDistrictService;

    public ElectoralDistrictResource(ElectoralDistrictService electoralDistrictService) {
        this.electoralDistrictService = electoralDistrictService;
    }

    /**
     * {@code POST  /electoral-districts} : Create a new electoralDistrict.
     *
     * @param electoralDistrict the electoralDistrict to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new electoralDistrict, or with status {@code 400 (Bad Request)} if the electoralDistrict has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/electoral-districts")
    public ResponseEntity<ElectoralDistrict> createElectoralDistrict(@RequestBody ElectoralDistrict electoralDistrict) throws URISyntaxException {
        log.debug("REST request to save ElectoralDistrict : {}", electoralDistrict);
        if (electoralDistrict.getId() != null) {
            throw new BadRequestAlertException("A new electoralDistrict cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ElectoralDistrict result = electoralDistrictService.save(electoralDistrict);
        return ResponseEntity.created(new URI("/api/electoral-districts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /electoral-districts} : Updates an existing electoralDistrict.
     *
     * @param electoralDistrict the electoralDistrict to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated electoralDistrict,
     * or with status {@code 400 (Bad Request)} if the electoralDistrict is not valid,
     * or with status {@code 500 (Internal Server Error)} if the electoralDistrict couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/electoral-districts")
    public ResponseEntity<ElectoralDistrict> updateElectoralDistrict(@RequestBody ElectoralDistrict electoralDistrict) throws URISyntaxException {
        log.debug("REST request to update ElectoralDistrict : {}", electoralDistrict);
        if (electoralDistrict.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ElectoralDistrict result = electoralDistrictService.save(electoralDistrict);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, electoralDistrict.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /electoral-districts} : get all the electoralDistricts.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of electoralDistricts in body.
     */
    @GetMapping("/electoral-districts")
    public ResponseEntity<List<ElectoralDistrict>> getAllElectoralDistricts(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of ElectoralDistricts");
        Page<ElectoralDistrict> page = electoralDistrictService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /electoral-districts/:id} : get the "id" electoralDistrict.
     *
     * @param id the id of the electoralDistrict to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the electoralDistrict, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/electoral-districts/{id}")
    public ResponseEntity<ElectoralDistrict> getElectoralDistrict(@PathVariable Long id) {
        log.debug("REST request to get ElectoralDistrict : {}", id);
        Optional<ElectoralDistrict> electoralDistrict = electoralDistrictService.findOne(id);
        return ResponseUtil.wrapOrNotFound(electoralDistrict);
    }

    /**
     * {@code DELETE  /electoral-districts/:id} : delete the "id" electoralDistrict.
     *
     * @param id the id of the electoralDistrict to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/electoral-districts/{id}")
    public ResponseEntity<Void> deleteElectoralDistrict(@PathVariable Long id) {
        log.debug("REST request to delete ElectoralDistrict : {}", id);
        electoralDistrictService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
