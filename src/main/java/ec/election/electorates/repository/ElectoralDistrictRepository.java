package ec.election.electorates.repository;

import ec.election.electorates.domain.ElectoralDistrict;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ElectoralDistrict entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ElectoralDistrictRepository extends JpaRepository<ElectoralDistrict, Long> {

}
