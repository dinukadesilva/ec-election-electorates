package ec.election.electorates.repository;

import ec.election.electorates.domain.PollingDivision;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PollingDivision entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PollingDivisionRepository extends JpaRepository<PollingDivision, Long> {

}
