package ec.election.electorates.repository;

import ec.election.electorates.domain.PollingStation;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PollingStation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PollingStationRepository extends JpaRepository<PollingStation, Long> {

}
