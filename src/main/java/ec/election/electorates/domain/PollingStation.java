package ec.election.electorates.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A PollingStation.
 */
@Entity
@Table(name = "polling_station")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PollingStation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "polling_station_address")
    private String pollingStationAddress;

    @ManyToOne
    @JsonIgnoreProperties("pollingStations")
    private PollingDivision pollingDivision;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPollingStationAddress() {
        return pollingStationAddress;
    }

    public PollingStation pollingStationAddress(String pollingStationAddress) {
        this.pollingStationAddress = pollingStationAddress;
        return this;
    }

    public void setPollingStationAddress(String pollingStationAddress) {
        this.pollingStationAddress = pollingStationAddress;
    }

    public PollingDivision getPollingDivision() {
        return pollingDivision;
    }

    public PollingStation pollingDivision(PollingDivision pollingDivision) {
        this.pollingDivision = pollingDivision;
        return this;
    }

    public void setPollingDivision(PollingDivision pollingDivision) {
        this.pollingDivision = pollingDivision;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PollingStation)) {
            return false;
        }
        return id != null && id.equals(((PollingStation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "PollingStation{" +
            "id=" + getId() +
            ", pollingStationAddress='" + getPollingStationAddress() + "'" +
            "}";
    }
}
