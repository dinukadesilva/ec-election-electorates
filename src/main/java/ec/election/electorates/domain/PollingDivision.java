package ec.election.electorates.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A PollingDivision.
 */
@Entity
@Table(name = "polling_division")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PollingDivision implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "polling_division_name")
    private String pollingDivisionName;

    @ManyToOne
    @JsonIgnoreProperties("pollingDivisions")
    private ElectoralDistrict electoralDistrict;

    @OneToMany(mappedBy = "pollingDivision")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<PollingStation> pollingStations = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPollingDivisionName() {
        return pollingDivisionName;
    }

    public PollingDivision pollingDivisionName(String pollingDivisionName) {
        this.pollingDivisionName = pollingDivisionName;
        return this;
    }

    public void setPollingDivisionName(String pollingDivisionName) {
        this.pollingDivisionName = pollingDivisionName;
    }

    public ElectoralDistrict getElectoralDistrict() {
        return electoralDistrict;
    }

    public PollingDivision electoralDistrict(ElectoralDistrict electoralDistrict) {
        this.electoralDistrict = electoralDistrict;
        return this;
    }

    public void setElectoralDistrict(ElectoralDistrict electoralDistrict) {
        this.electoralDistrict = electoralDistrict;
    }

    public Set<PollingStation> getPollingStations() {
        return pollingStations;
    }

    public PollingDivision pollingStations(Set<PollingStation> pollingStations) {
        this.pollingStations = pollingStations;
        return this;
    }

    public PollingDivision addPollingStation(PollingStation pollingStation) {
        this.pollingStations.add(pollingStation);
        pollingStation.setPollingDivision(this);
        return this;
    }

    public PollingDivision removePollingStation(PollingStation pollingStation) {
        this.pollingStations.remove(pollingStation);
        pollingStation.setPollingDivision(null);
        return this;
    }

    public void setPollingStations(Set<PollingStation> pollingStations) {
        this.pollingStations = pollingStations;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PollingDivision)) {
            return false;
        }
        return id != null && id.equals(((PollingDivision) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "PollingDivision{" +
            "id=" + getId() +
            ", pollingDivisionName='" + getPollingDivisionName() + "'" +
            "}";
    }
}
