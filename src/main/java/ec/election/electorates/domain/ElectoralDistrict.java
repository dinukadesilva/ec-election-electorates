package ec.election.electorates.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A ElectoralDistrict.
 */
@Entity
@Table(name = "electoral_district")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ElectoralDistrict implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "electoral_district_name")
    private String electoralDistrictName;

    @OneToMany(mappedBy = "electoralDistrict")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<PollingDivision> pollingDivisions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getElectoralDistrictName() {
        return electoralDistrictName;
    }

    public ElectoralDistrict electoralDistrictName(String electoralDistrictName) {
        this.electoralDistrictName = electoralDistrictName;
        return this;
    }

    public void setElectoralDistrictName(String electoralDistrictName) {
        this.electoralDistrictName = electoralDistrictName;
    }

    public Set<PollingDivision> getPollingDivisions() {
        return pollingDivisions;
    }

    public ElectoralDistrict pollingDivisions(Set<PollingDivision> pollingDivisions) {
        this.pollingDivisions = pollingDivisions;
        return this;
    }

    public ElectoralDistrict addPollingDivision(PollingDivision pollingDivision) {
        this.pollingDivisions.add(pollingDivision);
        pollingDivision.setElectoralDistrict(this);
        return this;
    }

    public ElectoralDistrict removePollingDivision(PollingDivision pollingDivision) {
        this.pollingDivisions.remove(pollingDivision);
        pollingDivision.setElectoralDistrict(null);
        return this;
    }

    public void setPollingDivisions(Set<PollingDivision> pollingDivisions) {
        this.pollingDivisions = pollingDivisions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ElectoralDistrict)) {
            return false;
        }
        return id != null && id.equals(((ElectoralDistrict) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ElectoralDistrict{" +
            "id=" + getId() +
            ", electoralDistrictName='" + getElectoralDistrictName() + "'" +
            "}";
    }
}
