import { IElectoralDistrict } from 'app/shared/model/electoral-district.model';
import { IPollingStation } from 'app/shared/model/polling-station.model';

export interface IPollingDivision {
  id?: number;
  pollingDivisionName?: string;
  electoralDistrict?: IElectoralDistrict;
  pollingStations?: IPollingStation[];
}

export const defaultValue: Readonly<IPollingDivision> = {};
