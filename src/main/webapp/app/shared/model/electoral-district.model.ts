import { IPollingDivision } from 'app/shared/model/polling-division.model';

export interface IElectoralDistrict {
  id?: number;
  electoralDistrictName?: string;
  pollingDivisions?: IPollingDivision[];
}

export const defaultValue: Readonly<IElectoralDistrict> = {};
