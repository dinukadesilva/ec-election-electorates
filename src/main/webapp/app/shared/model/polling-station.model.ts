import { IPollingDivision } from 'app/shared/model/polling-division.model';

export interface IPollingStation {
  id?: number;
  pollingStationAddress?: string;
  pollingDivision?: IPollingDivision;
}

export const defaultValue: Readonly<IPollingStation> = {};
