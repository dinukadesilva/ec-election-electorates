import React from 'react';
import { Switch } from 'react-router-dom';

// tslint:disable-next-line:no-unused-variable
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ElectoralDistrict from './electoral-district';
import PollingDivision from './polling-division';
import PollingStation from './polling-station';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}/electoral-district`} component={ElectoralDistrict} />
      <ErrorBoundaryRoute path={`${match.url}/polling-division`} component={PollingDivision} />
      <ErrorBoundaryRoute path={`${match.url}/polling-station`} component={PollingStation} />
      {/* jhipster-needle-add-route-path - JHipster will add routes here */}
    </Switch>
  </div>
);

export default Routes;
