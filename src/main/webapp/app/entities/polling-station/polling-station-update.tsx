import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IPollingDivision } from 'app/shared/model/polling-division.model';
import { getEntities as getPollingDivisions } from 'app/entities/polling-division/polling-division.reducer';
import { getEntity, updateEntity, createEntity, reset } from './polling-station.reducer';
import { IPollingStation } from 'app/shared/model/polling-station.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IPollingStationUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IPollingStationUpdateState {
  isNew: boolean;
  pollingDivisionId: string;
}

export class PollingStationUpdate extends React.Component<IPollingStationUpdateProps, IPollingStationUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      pollingDivisionId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getPollingDivisions();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { pollingStationEntity } = this.props;
      const entity = {
        ...pollingStationEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/polling-station');
  };

  render() {
    const { pollingStationEntity, pollingDivisions, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="electionElectoratesApp.pollingStation.home.createOrEditLabel">Create or edit a PollingStation</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : pollingStationEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="polling-station-id">ID</Label>
                    <AvInput id="polling-station-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="pollingStationAddressLabel" for="polling-station-pollingStationAddress">
                    Polling Station Address
                  </Label>
                  <AvField id="polling-station-pollingStationAddress" type="text" name="pollingStationAddress" />
                </AvGroup>
                <AvGroup>
                  <Label for="polling-station-pollingDivision">Polling Division</Label>
                  <AvInput id="polling-station-pollingDivision" type="select" className="form-control" name="pollingDivision.id">
                    <option value="" key="0" />
                    {pollingDivisions
                      ? pollingDivisions.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/polling-station" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  pollingDivisions: storeState.pollingDivision.entities,
  pollingStationEntity: storeState.pollingStation.entity,
  loading: storeState.pollingStation.loading,
  updating: storeState.pollingStation.updating,
  updateSuccess: storeState.pollingStation.updateSuccess
});

const mapDispatchToProps = {
  getPollingDivisions,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PollingStationUpdate);
