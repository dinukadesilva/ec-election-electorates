import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IPollingStation, defaultValue } from 'app/shared/model/polling-station.model';

export const ACTION_TYPES = {
  FETCH_POLLINGSTATION_LIST: 'pollingStation/FETCH_POLLINGSTATION_LIST',
  FETCH_POLLINGSTATION: 'pollingStation/FETCH_POLLINGSTATION',
  CREATE_POLLINGSTATION: 'pollingStation/CREATE_POLLINGSTATION',
  UPDATE_POLLINGSTATION: 'pollingStation/UPDATE_POLLINGSTATION',
  DELETE_POLLINGSTATION: 'pollingStation/DELETE_POLLINGSTATION',
  RESET: 'pollingStation/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IPollingStation>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type PollingStationState = Readonly<typeof initialState>;

// Reducer

export default (state: PollingStationState = initialState, action): PollingStationState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_POLLINGSTATION_LIST):
    case REQUEST(ACTION_TYPES.FETCH_POLLINGSTATION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_POLLINGSTATION):
    case REQUEST(ACTION_TYPES.UPDATE_POLLINGSTATION):
    case REQUEST(ACTION_TYPES.DELETE_POLLINGSTATION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_POLLINGSTATION_LIST):
    case FAILURE(ACTION_TYPES.FETCH_POLLINGSTATION):
    case FAILURE(ACTION_TYPES.CREATE_POLLINGSTATION):
    case FAILURE(ACTION_TYPES.UPDATE_POLLINGSTATION):
    case FAILURE(ACTION_TYPES.DELETE_POLLINGSTATION):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_POLLINGSTATION_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_POLLINGSTATION):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_POLLINGSTATION):
    case SUCCESS(ACTION_TYPES.UPDATE_POLLINGSTATION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_POLLINGSTATION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/polling-stations';

// Actions

export const getEntities: ICrudGetAllAction<IPollingStation> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_POLLINGSTATION_LIST,
    payload: axios.get<IPollingStation>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IPollingStation> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_POLLINGSTATION,
    payload: axios.get<IPollingStation>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IPollingStation> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_POLLINGSTATION,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IPollingStation> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_POLLINGSTATION,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IPollingStation> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_POLLINGSTATION,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
