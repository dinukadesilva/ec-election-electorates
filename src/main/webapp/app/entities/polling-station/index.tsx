import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import PollingStation from './polling-station';
import PollingStationDetail from './polling-station-detail';
import PollingStationUpdate from './polling-station-update';
import PollingStationDeleteDialog from './polling-station-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={PollingStationUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={PollingStationUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={PollingStationDetail} />
      <ErrorBoundaryRoute path={match.url} component={PollingStation} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={PollingStationDeleteDialog} />
  </>
);

export default Routes;
