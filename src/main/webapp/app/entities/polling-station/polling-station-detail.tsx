import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './polling-station.reducer';
import { IPollingStation } from 'app/shared/model/polling-station.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPollingStationDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class PollingStationDetail extends React.Component<IPollingStationDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { pollingStationEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            PollingStation [<b>{pollingStationEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="pollingStationAddress">Polling Station Address</span>
            </dt>
            <dd>{pollingStationEntity.pollingStationAddress}</dd>
            <dt>Polling Division</dt>
            <dd>{pollingStationEntity.pollingDivision ? pollingStationEntity.pollingDivision.id : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/polling-station" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/polling-station/${pollingStationEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ pollingStation }: IRootState) => ({
  pollingStationEntity: pollingStation.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PollingStationDetail);
