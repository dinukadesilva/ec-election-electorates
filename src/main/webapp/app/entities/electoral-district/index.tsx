import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ElectoralDistrict from './electoral-district';
import ElectoralDistrictDetail from './electoral-district-detail';
import ElectoralDistrictUpdate from './electoral-district-update';
import ElectoralDistrictDeleteDialog from './electoral-district-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ElectoralDistrictUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ElectoralDistrictUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ElectoralDistrictDetail} />
      <ErrorBoundaryRoute path={match.url} component={ElectoralDistrict} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ElectoralDistrictDeleteDialog} />
  </>
);

export default Routes;
