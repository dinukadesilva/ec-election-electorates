import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IElectoralDistrict, defaultValue } from 'app/shared/model/electoral-district.model';

export const ACTION_TYPES = {
  FETCH_ELECTORALDISTRICT_LIST: 'electoralDistrict/FETCH_ELECTORALDISTRICT_LIST',
  FETCH_ELECTORALDISTRICT: 'electoralDistrict/FETCH_ELECTORALDISTRICT',
  CREATE_ELECTORALDISTRICT: 'electoralDistrict/CREATE_ELECTORALDISTRICT',
  UPDATE_ELECTORALDISTRICT: 'electoralDistrict/UPDATE_ELECTORALDISTRICT',
  DELETE_ELECTORALDISTRICT: 'electoralDistrict/DELETE_ELECTORALDISTRICT',
  RESET: 'electoralDistrict/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IElectoralDistrict>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ElectoralDistrictState = Readonly<typeof initialState>;

// Reducer

export default (state: ElectoralDistrictState = initialState, action): ElectoralDistrictState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_ELECTORALDISTRICT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_ELECTORALDISTRICT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_ELECTORALDISTRICT):
    case REQUEST(ACTION_TYPES.UPDATE_ELECTORALDISTRICT):
    case REQUEST(ACTION_TYPES.DELETE_ELECTORALDISTRICT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_ELECTORALDISTRICT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_ELECTORALDISTRICT):
    case FAILURE(ACTION_TYPES.CREATE_ELECTORALDISTRICT):
    case FAILURE(ACTION_TYPES.UPDATE_ELECTORALDISTRICT):
    case FAILURE(ACTION_TYPES.DELETE_ELECTORALDISTRICT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_ELECTORALDISTRICT_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_ELECTORALDISTRICT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_ELECTORALDISTRICT):
    case SUCCESS(ACTION_TYPES.UPDATE_ELECTORALDISTRICT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_ELECTORALDISTRICT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/electoral-districts';

// Actions

export const getEntities: ICrudGetAllAction<IElectoralDistrict> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_ELECTORALDISTRICT_LIST,
    payload: axios.get<IElectoralDistrict>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IElectoralDistrict> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_ELECTORALDISTRICT,
    payload: axios.get<IElectoralDistrict>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IElectoralDistrict> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_ELECTORALDISTRICT,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IElectoralDistrict> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_ELECTORALDISTRICT,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IElectoralDistrict> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_ELECTORALDISTRICT,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
