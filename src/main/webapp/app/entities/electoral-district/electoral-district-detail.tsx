import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './electoral-district.reducer';
import { IElectoralDistrict } from 'app/shared/model/electoral-district.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IElectoralDistrictDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ElectoralDistrictDetail extends React.Component<IElectoralDistrictDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { electoralDistrictEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            ElectoralDistrict [<b>{electoralDistrictEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="electoralDistrictName">Electoral District Name</span>
            </dt>
            <dd>{electoralDistrictEntity.electoralDistrictName}</dd>
          </dl>
          <Button tag={Link} to="/entity/electoral-district" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/electoral-district/${electoralDistrictEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ electoralDistrict }: IRootState) => ({
  electoralDistrictEntity: electoralDistrict.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ElectoralDistrictDetail);
