import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IElectoralDistrict } from 'app/shared/model/electoral-district.model';
import { getEntities as getElectoralDistricts } from 'app/entities/electoral-district/electoral-district.reducer';
import { getEntity, updateEntity, createEntity, reset } from './polling-division.reducer';
import { IPollingDivision } from 'app/shared/model/polling-division.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IPollingDivisionUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IPollingDivisionUpdateState {
  isNew: boolean;
  electoralDistrictId: string;
}

export class PollingDivisionUpdate extends React.Component<IPollingDivisionUpdateProps, IPollingDivisionUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      electoralDistrictId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getElectoralDistricts();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { pollingDivisionEntity } = this.props;
      const entity = {
        ...pollingDivisionEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/polling-division');
  };

  render() {
    const { pollingDivisionEntity, electoralDistricts, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="electionElectoratesApp.pollingDivision.home.createOrEditLabel">Create or edit a PollingDivision</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : pollingDivisionEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="polling-division-id">ID</Label>
                    <AvInput id="polling-division-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="pollingDivisionNameLabel" for="polling-division-pollingDivisionName">
                    Polling Division Name
                  </Label>
                  <AvField id="polling-division-pollingDivisionName" type="text" name="pollingDivisionName" />
                </AvGroup>
                <AvGroup>
                  <Label for="polling-division-electoralDistrict">Electoral District</Label>
                  <AvInput id="polling-division-electoralDistrict" type="select" className="form-control" name="electoralDistrict.id">
                    <option value="" key="0" />
                    {electoralDistricts
                      ? electoralDistricts.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/polling-division" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  electoralDistricts: storeState.electoralDistrict.entities,
  pollingDivisionEntity: storeState.pollingDivision.entity,
  loading: storeState.pollingDivision.loading,
  updating: storeState.pollingDivision.updating,
  updateSuccess: storeState.pollingDivision.updateSuccess
});

const mapDispatchToProps = {
  getElectoralDistricts,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PollingDivisionUpdate);
