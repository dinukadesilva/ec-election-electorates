import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IPollingDivision, defaultValue } from 'app/shared/model/polling-division.model';

export const ACTION_TYPES = {
  FETCH_POLLINGDIVISION_LIST: 'pollingDivision/FETCH_POLLINGDIVISION_LIST',
  FETCH_POLLINGDIVISION: 'pollingDivision/FETCH_POLLINGDIVISION',
  CREATE_POLLINGDIVISION: 'pollingDivision/CREATE_POLLINGDIVISION',
  UPDATE_POLLINGDIVISION: 'pollingDivision/UPDATE_POLLINGDIVISION',
  DELETE_POLLINGDIVISION: 'pollingDivision/DELETE_POLLINGDIVISION',
  RESET: 'pollingDivision/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IPollingDivision>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type PollingDivisionState = Readonly<typeof initialState>;

// Reducer

export default (state: PollingDivisionState = initialState, action): PollingDivisionState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_POLLINGDIVISION_LIST):
    case REQUEST(ACTION_TYPES.FETCH_POLLINGDIVISION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_POLLINGDIVISION):
    case REQUEST(ACTION_TYPES.UPDATE_POLLINGDIVISION):
    case REQUEST(ACTION_TYPES.DELETE_POLLINGDIVISION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_POLLINGDIVISION_LIST):
    case FAILURE(ACTION_TYPES.FETCH_POLLINGDIVISION):
    case FAILURE(ACTION_TYPES.CREATE_POLLINGDIVISION):
    case FAILURE(ACTION_TYPES.UPDATE_POLLINGDIVISION):
    case FAILURE(ACTION_TYPES.DELETE_POLLINGDIVISION):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_POLLINGDIVISION_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_POLLINGDIVISION):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_POLLINGDIVISION):
    case SUCCESS(ACTION_TYPES.UPDATE_POLLINGDIVISION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_POLLINGDIVISION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/polling-divisions';

// Actions

export const getEntities: ICrudGetAllAction<IPollingDivision> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_POLLINGDIVISION_LIST,
    payload: axios.get<IPollingDivision>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IPollingDivision> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_POLLINGDIVISION,
    payload: axios.get<IPollingDivision>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IPollingDivision> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_POLLINGDIVISION,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IPollingDivision> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_POLLINGDIVISION,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IPollingDivision> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_POLLINGDIVISION,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
