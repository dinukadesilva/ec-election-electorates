import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import PollingDivision from './polling-division';
import PollingDivisionDetail from './polling-division-detail';
import PollingDivisionUpdate from './polling-division-update';
import PollingDivisionDeleteDialog from './polling-division-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={PollingDivisionUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={PollingDivisionUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={PollingDivisionDetail} />
      <ErrorBoundaryRoute path={match.url} component={PollingDivision} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={PollingDivisionDeleteDialog} />
  </>
);

export default Routes;
