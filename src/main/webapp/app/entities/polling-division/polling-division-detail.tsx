import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './polling-division.reducer';
import { IPollingDivision } from 'app/shared/model/polling-division.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPollingDivisionDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class PollingDivisionDetail extends React.Component<IPollingDivisionDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { pollingDivisionEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            PollingDivision [<b>{pollingDivisionEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="pollingDivisionName">Polling Division Name</span>
            </dt>
            <dd>{pollingDivisionEntity.pollingDivisionName}</dd>
            <dt>Electoral District</dt>
            <dd>{pollingDivisionEntity.electoralDistrict ? pollingDivisionEntity.electoralDistrict.id : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/polling-division" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/polling-division/${pollingDivisionEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ pollingDivision }: IRootState) => ({
  pollingDivisionEntity: pollingDivision.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PollingDivisionDetail);
