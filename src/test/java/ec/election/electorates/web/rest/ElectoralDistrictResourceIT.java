package ec.election.electorates.web.rest;

import ec.election.electorates.ElectionElectoratesApp;
import ec.election.electorates.domain.ElectoralDistrict;
import ec.election.electorates.repository.ElectoralDistrictRepository;
import ec.election.electorates.service.ElectoralDistrictService;
import ec.election.electorates.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static ec.election.electorates.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ElectoralDistrictResource} REST controller.
 */
@SpringBootTest(classes = ElectionElectoratesApp.class)
public class ElectoralDistrictResourceIT {

    private static final String DEFAULT_ELECTORAL_DISTRICT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ELECTORAL_DISTRICT_NAME = "BBBBBBBBBB";

    @Autowired
    private ElectoralDistrictRepository electoralDistrictRepository;

    @Autowired
    private ElectoralDistrictService electoralDistrictService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restElectoralDistrictMockMvc;

    private ElectoralDistrict electoralDistrict;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ElectoralDistrictResource electoralDistrictResource = new ElectoralDistrictResource(electoralDistrictService);
        this.restElectoralDistrictMockMvc = MockMvcBuilders.standaloneSetup(electoralDistrictResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ElectoralDistrict createEntity(EntityManager em) {
        ElectoralDistrict electoralDistrict = new ElectoralDistrict()
            .electoralDistrictName(DEFAULT_ELECTORAL_DISTRICT_NAME);
        return electoralDistrict;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ElectoralDistrict createUpdatedEntity(EntityManager em) {
        ElectoralDistrict electoralDistrict = new ElectoralDistrict()
            .electoralDistrictName(UPDATED_ELECTORAL_DISTRICT_NAME);
        return electoralDistrict;
    }

    @BeforeEach
    public void initTest() {
        electoralDistrict = createEntity(em);
    }

    @Test
    @Transactional
    public void createElectoralDistrict() throws Exception {
        int databaseSizeBeforeCreate = electoralDistrictRepository.findAll().size();

        // Create the ElectoralDistrict
        restElectoralDistrictMockMvc.perform(post("/api/electoral-districts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(electoralDistrict)))
            .andExpect(status().isCreated());

        // Validate the ElectoralDistrict in the database
        List<ElectoralDistrict> electoralDistrictList = electoralDistrictRepository.findAll();
        assertThat(electoralDistrictList).hasSize(databaseSizeBeforeCreate + 1);
        ElectoralDistrict testElectoralDistrict = electoralDistrictList.get(electoralDistrictList.size() - 1);
        assertThat(testElectoralDistrict.getElectoralDistrictName()).isEqualTo(DEFAULT_ELECTORAL_DISTRICT_NAME);
    }

    @Test
    @Transactional
    public void createElectoralDistrictWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = electoralDistrictRepository.findAll().size();

        // Create the ElectoralDistrict with an existing ID
        electoralDistrict.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restElectoralDistrictMockMvc.perform(post("/api/electoral-districts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(electoralDistrict)))
            .andExpect(status().isBadRequest());

        // Validate the ElectoralDistrict in the database
        List<ElectoralDistrict> electoralDistrictList = electoralDistrictRepository.findAll();
        assertThat(electoralDistrictList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllElectoralDistricts() throws Exception {
        // Initialize the database
        electoralDistrictRepository.saveAndFlush(electoralDistrict);

        // Get all the electoralDistrictList
        restElectoralDistrictMockMvc.perform(get("/api/electoral-districts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(electoralDistrict.getId().intValue())))
            .andExpect(jsonPath("$.[*].electoralDistrictName").value(hasItem(DEFAULT_ELECTORAL_DISTRICT_NAME.toString())));
    }
    
    @Test
    @Transactional
    public void getElectoralDistrict() throws Exception {
        // Initialize the database
        electoralDistrictRepository.saveAndFlush(electoralDistrict);

        // Get the electoralDistrict
        restElectoralDistrictMockMvc.perform(get("/api/electoral-districts/{id}", electoralDistrict.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(electoralDistrict.getId().intValue()))
            .andExpect(jsonPath("$.electoralDistrictName").value(DEFAULT_ELECTORAL_DISTRICT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingElectoralDistrict() throws Exception {
        // Get the electoralDistrict
        restElectoralDistrictMockMvc.perform(get("/api/electoral-districts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateElectoralDistrict() throws Exception {
        // Initialize the database
        electoralDistrictService.save(electoralDistrict);

        int databaseSizeBeforeUpdate = electoralDistrictRepository.findAll().size();

        // Update the electoralDistrict
        ElectoralDistrict updatedElectoralDistrict = electoralDistrictRepository.findById(electoralDistrict.getId()).get();
        // Disconnect from session so that the updates on updatedElectoralDistrict are not directly saved in db
        em.detach(updatedElectoralDistrict);
        updatedElectoralDistrict
            .electoralDistrictName(UPDATED_ELECTORAL_DISTRICT_NAME);

        restElectoralDistrictMockMvc.perform(put("/api/electoral-districts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedElectoralDistrict)))
            .andExpect(status().isOk());

        // Validate the ElectoralDistrict in the database
        List<ElectoralDistrict> electoralDistrictList = electoralDistrictRepository.findAll();
        assertThat(electoralDistrictList).hasSize(databaseSizeBeforeUpdate);
        ElectoralDistrict testElectoralDistrict = electoralDistrictList.get(electoralDistrictList.size() - 1);
        assertThat(testElectoralDistrict.getElectoralDistrictName()).isEqualTo(UPDATED_ELECTORAL_DISTRICT_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingElectoralDistrict() throws Exception {
        int databaseSizeBeforeUpdate = electoralDistrictRepository.findAll().size();

        // Create the ElectoralDistrict

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restElectoralDistrictMockMvc.perform(put("/api/electoral-districts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(electoralDistrict)))
            .andExpect(status().isBadRequest());

        // Validate the ElectoralDistrict in the database
        List<ElectoralDistrict> electoralDistrictList = electoralDistrictRepository.findAll();
        assertThat(electoralDistrictList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteElectoralDistrict() throws Exception {
        // Initialize the database
        electoralDistrictService.save(electoralDistrict);

        int databaseSizeBeforeDelete = electoralDistrictRepository.findAll().size();

        // Delete the electoralDistrict
        restElectoralDistrictMockMvc.perform(delete("/api/electoral-districts/{id}", electoralDistrict.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ElectoralDistrict> electoralDistrictList = electoralDistrictRepository.findAll();
        assertThat(electoralDistrictList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ElectoralDistrict.class);
        ElectoralDistrict electoralDistrict1 = new ElectoralDistrict();
        electoralDistrict1.setId(1L);
        ElectoralDistrict electoralDistrict2 = new ElectoralDistrict();
        electoralDistrict2.setId(electoralDistrict1.getId());
        assertThat(electoralDistrict1).isEqualTo(electoralDistrict2);
        electoralDistrict2.setId(2L);
        assertThat(electoralDistrict1).isNotEqualTo(electoralDistrict2);
        electoralDistrict1.setId(null);
        assertThat(electoralDistrict1).isNotEqualTo(electoralDistrict2);
    }
}
