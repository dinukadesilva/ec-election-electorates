package ec.election.electorates.web.rest;

import ec.election.electorates.ElectionElectoratesApp;
import ec.election.electorates.domain.PollingStation;
import ec.election.electorates.repository.PollingStationRepository;
import ec.election.electorates.service.PollingStationService;
import ec.election.electorates.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static ec.election.electorates.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link PollingStationResource} REST controller.
 */
@SpringBootTest(classes = ElectionElectoratesApp.class)
public class PollingStationResourceIT {

    private static final String DEFAULT_POLLING_STATION_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_POLLING_STATION_ADDRESS = "BBBBBBBBBB";

    @Autowired
    private PollingStationRepository pollingStationRepository;

    @Autowired
    private PollingStationService pollingStationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPollingStationMockMvc;

    private PollingStation pollingStation;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PollingStationResource pollingStationResource = new PollingStationResource(pollingStationService);
        this.restPollingStationMockMvc = MockMvcBuilders.standaloneSetup(pollingStationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PollingStation createEntity(EntityManager em) {
        PollingStation pollingStation = new PollingStation()
            .pollingStationAddress(DEFAULT_POLLING_STATION_ADDRESS);
        return pollingStation;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PollingStation createUpdatedEntity(EntityManager em) {
        PollingStation pollingStation = new PollingStation()
            .pollingStationAddress(UPDATED_POLLING_STATION_ADDRESS);
        return pollingStation;
    }

    @BeforeEach
    public void initTest() {
        pollingStation = createEntity(em);
    }

    @Test
    @Transactional
    public void createPollingStation() throws Exception {
        int databaseSizeBeforeCreate = pollingStationRepository.findAll().size();

        // Create the PollingStation
        restPollingStationMockMvc.perform(post("/api/polling-stations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pollingStation)))
            .andExpect(status().isCreated());

        // Validate the PollingStation in the database
        List<PollingStation> pollingStationList = pollingStationRepository.findAll();
        assertThat(pollingStationList).hasSize(databaseSizeBeforeCreate + 1);
        PollingStation testPollingStation = pollingStationList.get(pollingStationList.size() - 1);
        assertThat(testPollingStation.getPollingStationAddress()).isEqualTo(DEFAULT_POLLING_STATION_ADDRESS);
    }

    @Test
    @Transactional
    public void createPollingStationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pollingStationRepository.findAll().size();

        // Create the PollingStation with an existing ID
        pollingStation.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPollingStationMockMvc.perform(post("/api/polling-stations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pollingStation)))
            .andExpect(status().isBadRequest());

        // Validate the PollingStation in the database
        List<PollingStation> pollingStationList = pollingStationRepository.findAll();
        assertThat(pollingStationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPollingStations() throws Exception {
        // Initialize the database
        pollingStationRepository.saveAndFlush(pollingStation);

        // Get all the pollingStationList
        restPollingStationMockMvc.perform(get("/api/polling-stations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pollingStation.getId().intValue())))
            .andExpect(jsonPath("$.[*].pollingStationAddress").value(hasItem(DEFAULT_POLLING_STATION_ADDRESS.toString())));
    }
    
    @Test
    @Transactional
    public void getPollingStation() throws Exception {
        // Initialize the database
        pollingStationRepository.saveAndFlush(pollingStation);

        // Get the pollingStation
        restPollingStationMockMvc.perform(get("/api/polling-stations/{id}", pollingStation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pollingStation.getId().intValue()))
            .andExpect(jsonPath("$.pollingStationAddress").value(DEFAULT_POLLING_STATION_ADDRESS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPollingStation() throws Exception {
        // Get the pollingStation
        restPollingStationMockMvc.perform(get("/api/polling-stations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePollingStation() throws Exception {
        // Initialize the database
        pollingStationService.save(pollingStation);

        int databaseSizeBeforeUpdate = pollingStationRepository.findAll().size();

        // Update the pollingStation
        PollingStation updatedPollingStation = pollingStationRepository.findById(pollingStation.getId()).get();
        // Disconnect from session so that the updates on updatedPollingStation are not directly saved in db
        em.detach(updatedPollingStation);
        updatedPollingStation
            .pollingStationAddress(UPDATED_POLLING_STATION_ADDRESS);

        restPollingStationMockMvc.perform(put("/api/polling-stations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPollingStation)))
            .andExpect(status().isOk());

        // Validate the PollingStation in the database
        List<PollingStation> pollingStationList = pollingStationRepository.findAll();
        assertThat(pollingStationList).hasSize(databaseSizeBeforeUpdate);
        PollingStation testPollingStation = pollingStationList.get(pollingStationList.size() - 1);
        assertThat(testPollingStation.getPollingStationAddress()).isEqualTo(UPDATED_POLLING_STATION_ADDRESS);
    }

    @Test
    @Transactional
    public void updateNonExistingPollingStation() throws Exception {
        int databaseSizeBeforeUpdate = pollingStationRepository.findAll().size();

        // Create the PollingStation

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPollingStationMockMvc.perform(put("/api/polling-stations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pollingStation)))
            .andExpect(status().isBadRequest());

        // Validate the PollingStation in the database
        List<PollingStation> pollingStationList = pollingStationRepository.findAll();
        assertThat(pollingStationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePollingStation() throws Exception {
        // Initialize the database
        pollingStationService.save(pollingStation);

        int databaseSizeBeforeDelete = pollingStationRepository.findAll().size();

        // Delete the pollingStation
        restPollingStationMockMvc.perform(delete("/api/polling-stations/{id}", pollingStation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<PollingStation> pollingStationList = pollingStationRepository.findAll();
        assertThat(pollingStationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PollingStation.class);
        PollingStation pollingStation1 = new PollingStation();
        pollingStation1.setId(1L);
        PollingStation pollingStation2 = new PollingStation();
        pollingStation2.setId(pollingStation1.getId());
        assertThat(pollingStation1).isEqualTo(pollingStation2);
        pollingStation2.setId(2L);
        assertThat(pollingStation1).isNotEqualTo(pollingStation2);
        pollingStation1.setId(null);
        assertThat(pollingStation1).isNotEqualTo(pollingStation2);
    }
}
