package ec.election.electorates.web.rest;

import ec.election.electorates.ElectionElectoratesApp;
import ec.election.electorates.domain.PollingDivision;
import ec.election.electorates.repository.PollingDivisionRepository;
import ec.election.electorates.service.PollingDivisionService;
import ec.election.electorates.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static ec.election.electorates.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link PollingDivisionResource} REST controller.
 */
@SpringBootTest(classes = ElectionElectoratesApp.class)
public class PollingDivisionResourceIT {

    private static final String DEFAULT_POLLING_DIVISION_NAME = "AAAAAAAAAA";
    private static final String UPDATED_POLLING_DIVISION_NAME = "BBBBBBBBBB";

    @Autowired
    private PollingDivisionRepository pollingDivisionRepository;

    @Autowired
    private PollingDivisionService pollingDivisionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPollingDivisionMockMvc;

    private PollingDivision pollingDivision;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PollingDivisionResource pollingDivisionResource = new PollingDivisionResource(pollingDivisionService);
        this.restPollingDivisionMockMvc = MockMvcBuilders.standaloneSetup(pollingDivisionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PollingDivision createEntity(EntityManager em) {
        PollingDivision pollingDivision = new PollingDivision()
            .pollingDivisionName(DEFAULT_POLLING_DIVISION_NAME);
        return pollingDivision;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PollingDivision createUpdatedEntity(EntityManager em) {
        PollingDivision pollingDivision = new PollingDivision()
            .pollingDivisionName(UPDATED_POLLING_DIVISION_NAME);
        return pollingDivision;
    }

    @BeforeEach
    public void initTest() {
        pollingDivision = createEntity(em);
    }

    @Test
    @Transactional
    public void createPollingDivision() throws Exception {
        int databaseSizeBeforeCreate = pollingDivisionRepository.findAll().size();

        // Create the PollingDivision
        restPollingDivisionMockMvc.perform(post("/api/polling-divisions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pollingDivision)))
            .andExpect(status().isCreated());

        // Validate the PollingDivision in the database
        List<PollingDivision> pollingDivisionList = pollingDivisionRepository.findAll();
        assertThat(pollingDivisionList).hasSize(databaseSizeBeforeCreate + 1);
        PollingDivision testPollingDivision = pollingDivisionList.get(pollingDivisionList.size() - 1);
        assertThat(testPollingDivision.getPollingDivisionName()).isEqualTo(DEFAULT_POLLING_DIVISION_NAME);
    }

    @Test
    @Transactional
    public void createPollingDivisionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pollingDivisionRepository.findAll().size();

        // Create the PollingDivision with an existing ID
        pollingDivision.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPollingDivisionMockMvc.perform(post("/api/polling-divisions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pollingDivision)))
            .andExpect(status().isBadRequest());

        // Validate the PollingDivision in the database
        List<PollingDivision> pollingDivisionList = pollingDivisionRepository.findAll();
        assertThat(pollingDivisionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPollingDivisions() throws Exception {
        // Initialize the database
        pollingDivisionRepository.saveAndFlush(pollingDivision);

        // Get all the pollingDivisionList
        restPollingDivisionMockMvc.perform(get("/api/polling-divisions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pollingDivision.getId().intValue())))
            .andExpect(jsonPath("$.[*].pollingDivisionName").value(hasItem(DEFAULT_POLLING_DIVISION_NAME.toString())));
    }
    
    @Test
    @Transactional
    public void getPollingDivision() throws Exception {
        // Initialize the database
        pollingDivisionRepository.saveAndFlush(pollingDivision);

        // Get the pollingDivision
        restPollingDivisionMockMvc.perform(get("/api/polling-divisions/{id}", pollingDivision.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pollingDivision.getId().intValue()))
            .andExpect(jsonPath("$.pollingDivisionName").value(DEFAULT_POLLING_DIVISION_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPollingDivision() throws Exception {
        // Get the pollingDivision
        restPollingDivisionMockMvc.perform(get("/api/polling-divisions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePollingDivision() throws Exception {
        // Initialize the database
        pollingDivisionService.save(pollingDivision);

        int databaseSizeBeforeUpdate = pollingDivisionRepository.findAll().size();

        // Update the pollingDivision
        PollingDivision updatedPollingDivision = pollingDivisionRepository.findById(pollingDivision.getId()).get();
        // Disconnect from session so that the updates on updatedPollingDivision are not directly saved in db
        em.detach(updatedPollingDivision);
        updatedPollingDivision
            .pollingDivisionName(UPDATED_POLLING_DIVISION_NAME);

        restPollingDivisionMockMvc.perform(put("/api/polling-divisions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPollingDivision)))
            .andExpect(status().isOk());

        // Validate the PollingDivision in the database
        List<PollingDivision> pollingDivisionList = pollingDivisionRepository.findAll();
        assertThat(pollingDivisionList).hasSize(databaseSizeBeforeUpdate);
        PollingDivision testPollingDivision = pollingDivisionList.get(pollingDivisionList.size() - 1);
        assertThat(testPollingDivision.getPollingDivisionName()).isEqualTo(UPDATED_POLLING_DIVISION_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingPollingDivision() throws Exception {
        int databaseSizeBeforeUpdate = pollingDivisionRepository.findAll().size();

        // Create the PollingDivision

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPollingDivisionMockMvc.perform(put("/api/polling-divisions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pollingDivision)))
            .andExpect(status().isBadRequest());

        // Validate the PollingDivision in the database
        List<PollingDivision> pollingDivisionList = pollingDivisionRepository.findAll();
        assertThat(pollingDivisionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePollingDivision() throws Exception {
        // Initialize the database
        pollingDivisionService.save(pollingDivision);

        int databaseSizeBeforeDelete = pollingDivisionRepository.findAll().size();

        // Delete the pollingDivision
        restPollingDivisionMockMvc.perform(delete("/api/polling-divisions/{id}", pollingDivision.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<PollingDivision> pollingDivisionList = pollingDivisionRepository.findAll();
        assertThat(pollingDivisionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PollingDivision.class);
        PollingDivision pollingDivision1 = new PollingDivision();
        pollingDivision1.setId(1L);
        PollingDivision pollingDivision2 = new PollingDivision();
        pollingDivision2.setId(pollingDivision1.getId());
        assertThat(pollingDivision1).isEqualTo(pollingDivision2);
        pollingDivision2.setId(2L);
        assertThat(pollingDivision1).isNotEqualTo(pollingDivision2);
        pollingDivision1.setId(null);
        assertThat(pollingDivision1).isNotEqualTo(pollingDivision2);
    }
}
